extern crate crc;

use self::crc::{crc32};
use std::io::{Read, Write, BufReader, BufWriter};
use std::fs::File;
use args::Arguments;

#[derive(PartialEq)]
pub enum EncMode {
    OFB,
    CTR,
}

fn into_bytes(x: u32) -> [u8; 4] {
    let b1 : u8 = ((x >> 24) & 0xff) as u8;
    let b2 : u8 = ((x >> 16) & 0xff) as u8;
    let b3 : u8 = ((x >> 8) & 0xff) as u8;
    let b4 : u8 = (x & 0xff) as u8;
    return [b1, b2, b3, b4]
}

// key size is in bytes
fn get_key(password: &[u8], keysize: usize) -> Vec<u8> {
    let key0 = into_bytes(crc32::checksum_ieee(password));
    let blocks = keysize/4;
    let blocks = if blocks*4 == keysize { blocks } else { blocks + 1 }; // account for blocks with funny sizes
    let mut result = Vec::new();
    for _ in 0..blocks {
        result.extend_from_slice(&key0);
    }
    result.truncate(keysize);
    result
}

fn xor(a: &[u8], b: &[u8]) -> Vec<u8> {
    let mut result : Vec<u8> = Vec::with_capacity(a.len());
    for (i, j) in a.iter().zip(b.iter().cycle()) {
        result.push(i ^ j);
    }
    result
}

// our block cypher
fn crypt_block(block: &[u8], key: &[u8], encrypt: bool) -> Vec<u8> {
    if encrypt {
        let mut result = xor(block, key);
        result.rotate_right(1);
        result
    } else {
        let mut result = Vec::from(block);
        result.rotate_left(1);
        xor(&result, key)
    }
}

// Nor Read nor BufRead support iterating in chunks - this allows that, kind of.
// TODO would be to make this an iterator
fn read_block(input: &mut Read, output: &mut Vec<u8>) -> bool {
    if let Err(_) = input.read_exact(output) {
        output.clear();
        input.read_to_end(output).unwrap();
        return false;
    }
    true
}

fn ofb_mode(iv: &[u8], key: &[u8], input: &mut Read, output: &mut Write) {
    let mut cipher_input = Vec::new(); // input of the block cipher, initialized to iv.
    cipher_input.extend_from_slice(iv);
    let mut in_block = vec![0; key.len()]; // user input - either ciphertext when decrypting or plaintext when encrypting.
    loop {
        let yet_available = read_block(input, &mut in_block);
        if in_block.is_empty() {
            break;
        }
        let cipher_output = crypt_block(&cipher_input, key, true);
        cipher_input.clone_from_slice(&cipher_output);
        output.write_all(&xor(&in_block, &cipher_output)).unwrap();
        if !yet_available {
            break;
        }
    }
}

fn ctr_mode(nonce: &[u8], key: &[u8], input: &mut Read, output: &mut Write) {
    // block cipher input is initialized by xoring nonce with ctr extended to 32 bits and duplicated as
    // many times as needed to fill the block; this implies that nonce must be random and that we can
    // encrypt at most 2^32 blocks per nonce this way.
    let mut ctr = 0u32;
    let mut in_block = vec![0; key.len()];
    in_block.extend_from_slice(nonce); // this gets overwritten by read_block
    loop {
        let yet_available = read_block(input, &mut in_block);
        if in_block.is_empty() {
            break;
        }
        let cipher_output = crypt_block(&xor(nonce, &into_bytes(ctr)), key, true);
        output.write_all(&xor(&in_block, &cipher_output)).unwrap();
        if !yet_available {
            break;
        }
        ctr += 1;
    }
}

pub fn do_crypt(args: Arguments) {
    let blocksize = args.blocksize/8; // convert to bytes
    let mut iv_reader = BufReader::new(File::open(&args.iv_path).unwrap());
    let mut iv = Vec::new();
    iv_reader.read_to_end(&mut iv).unwrap();
    let mut input_reader = BufReader::new(File::open(&args.input_path).unwrap());
    let mut output_writer = BufWriter::new(File::create(&args.output_path).unwrap());
    let handler = match args.mode {
        EncMode::OFB => ofb_mode,
        EncMode::CTR => ctr_mode,
    };
    let key = get_key(args.password.as_bytes(), blocksize);
    iv.truncate(key.len());
    // encryption/decryption don't matter for the implemented modes
    handler(&iv, &key, &mut input_reader, &mut output_writer);
}

#[cfg(test)]
mod tests {
use crypto::*;

#[test]
fn key_has_correct_size() {
    let pass = b"TEST";
    let keysize = 16;
    let key = get_key(pass, keysize);
    assert_eq!(key.len(), keysize);
}

#[test]
fn can_process_single_block() {
    let pass = b"TEST";
    let keysize = 16;
    let key = get_key(pass, keysize);
    let plain = b"YELLOW SUBMARINE";
    let cipher = crypt_block(plain, &key, true);
    let decipher = crypt_block(&cipher, &key, false);
    assert_eq!(decipher, plain);
}

#[test]
fn ofb_works() {
    let pass = b"TESTME";
    let keysize = 16;
    let key = get_key(pass, keysize);
    let iv = [0; 16];
    let plain = b"And the Hippos Were Boiled in Their Tanks...........".to_vec();
    let mut target = &plain[..];
    let mut cipher: Vec<u8> = Vec::new();
    let mut decipher: Vec<u8> = Vec::new();
    ofb_mode(&iv, &key, &mut target, &mut cipher);
    let mut cipher = &cipher[..];
    ofb_mode(&iv, &key, &mut cipher, &mut decipher);
    assert_eq!(decipher, plain);
}

#[test]
fn ctr_works() {
    let pass = b"TESTME";
    let keysize = 32;
    let key = get_key(pass, keysize);
    let iv = [0; 32];
    let plain = b"And the Hippos Were Boiled in Their Tanks...........".to_vec();
    let mut target = &plain[..];
    let mut cipher: Vec<u8> = Vec::new();
    let mut decipher: Vec<u8> = Vec::new();
    ctr_mode(&iv, &key, &mut target, &mut cipher);
    let mut cipher = &cipher[..];
    ctr_mode(&iv, &key, &mut cipher, &mut decipher);
    assert_eq!(decipher, plain);
}


}
