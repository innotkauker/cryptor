pub mod args;
pub mod crypto;

fn main() {
    let parameters = args::parse_args().unwrap();
    crypto::do_crypt(parameters);
}
