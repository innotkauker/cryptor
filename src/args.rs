extern crate argparse;

use self::argparse::{ArgumentParser, Store, StoreTrue, StoreFalse};
use std::str::FromStr;
use std::fs::File;
use std::io::{self, Write, BufRead};
use std::error::Error;
use crypto::EncMode;

impl FromStr for EncMode {
    type Err = String;
    fn from_str(s: &str) -> Result<EncMode, Self::Err> {
        match s {
            "OFB" => Ok(EncMode::OFB),
            "CTR" => Ok(EncMode::CTR),
            _ => Err(String::from("Unknown mode")),
        }
    }
}

pub struct Arguments {
    pub blocksize: usize,
    pub mode: EncMode,
    pub encrypt: bool,
    pub iv_path: String,
    pub input_path: String,
    pub output_path: String,
    pub password: String,
}

pub fn parse_args() -> Result<Arguments, Box<Error>> {
    let mut res = Arguments {
        blocksize: 128,
        mode: EncMode::OFB,
        encrypt: true,
        iv_path: String::new(),
        input_path: String::new(),
        output_path: String::new(),
        password: String::new(),
    };
    {
        let mut ap = ArgumentParser::new();
        ap.set_description("Encrypt or decrypt a file.");
        ap.refer(&mut res.blocksize).add_option(&["-b", "--blocksize"], Store, "Block size; default is 128 bits.");
        ap.refer(&mut res.mode).add_option(&["-m", "--mode"], Store, "Type (OFB or CTR) to use; default is OFB.");
        ap.refer(&mut res.encrypt).add_option(&["-e", "--encrypt"], StoreTrue, "Encrypt.")
                                   .add_option(&["-d", "--decrypt"], StoreFalse, "Decrypt; default.");
        ap.refer(&mut res.iv_path).add_option(&["-v", "--iv"], Store, "Path to initialization vector.").required();
        ap.refer(&mut res.input_path).add_option(&["-i", "--input"], Store, "Path to input file.").required();
        ap.refer(&mut res.output_path).add_option(&["-o", "--output"], Store, "Path to output file.").required();
        ap.parse_args_or_exit();
    }
    print!("Password: ");
    io::stdout().flush().unwrap();
    let stdin = io::stdin();
    res.password = stdin.lock().lines().next().unwrap().unwrap();
    verify_args(res)
}

fn verify_args(args: Arguments) -> Result<Arguments, Box<Error>> {
    if args.blocksize % 32 != 0 {
        return Err(From::from("Wrong block size"));
    }
    File::open(&args.iv_path)?;
    File::open(&args.input_path)?;
    File::create(&args.output_path)?;
    Ok(args)
}
