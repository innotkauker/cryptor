# Cryptor

A cross-platform rust tool to encrypt files using a custom block cipher. CTR and OFB modes are supported.